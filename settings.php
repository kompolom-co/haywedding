<?php
/**
 * Main config file
 * @package Easyland
 * @version 0.3.3
 */

/**
 *  Список адресатов 
 *  Адреса электронной почты, на которые будут отправлены заявки
 *  перечисляются через запятую, в кавычках
 *  @example $MY_EMAIL = array("mail@email.ru","other@mail.com");
 *  @type array
 */
$MY_EMAIL = array("haywedding1@gmail.com","test@kompolom.ru");

/**
 * Электронный адрес отправителя
 * @type string
 */
define('FROM', "site@haywedding.ru");

/**
 * SMTP сервер
 * обязательно для отправки через smtp
 * Нужен ли в конкретном случае зависит от хостера
 * Если соединение шифруется, нужно указать протокол
 * @example "ssl://smtp.yandex.ru"
 * @type string
 */
define('SERVER', "mx1.hostinger.ru");

/**
 * Логин на SMTP сервере
 * Обычно совпадает с адресом отправителя
 * @type string
 */
define('LOGIN', "site@haywedding.ru");

/**
 * Пароль на SMTP сервере
 * Обычно совпадает с паролем почтового ящика отправителя
 * @type string
 */
define('PW', "jcDN8SeyA");

/**
 * Использовать SMTP?
 * @type bool
 * @example define(USE_SMTP,true);
 * @default false
 */
define('USE_SMTP', false);

/**
 * Порт на SMTP сервере
 * @type int
 * @default 25
 */
define('SMTP_PORT', 465);

/**
 * Имя сайта
 * Может использоваться в title или в теме письма
 * @type string
 * @required no
 */
define('SITENAME', 'Haywedding');

/**
 * Номер Яндекс-метрики
 * @type int
 * @required no
 */
define('METRICA', 25841045);

/**
 * Путь к БД IP адресов
 */
define('GEODB', 'lib/SxGeoCity.dat');

/**
 * Сообщение по умолчанию при успешной отправке заявки
 * @type string
 */
define('MSG_SUCCESS', 'Мы свяжемся с вами в ближайшее время');
//TODO: сообщение о сбое отпрвки
$formsrc = json_decode(file_get_contents('forms.json'), true);
$fieldsrc = json_decode(file_get_contents('fields.json'), true);
$content = json_decode(file_get_contents('content.json'), true);
ini_set('display_errors', false);

/**
 * метки рекламных кампаний
 */
$utmmarks = array(
    'utm_source'=>'Источник перехода по метке (utm_source)',
    'utm_medium'=>'Тип трафика(utm_medium)',
    'utm_campaign'=>'Кампания(utm_campaign)',
    'utm_term'=>'Ключевые слова(utm_term)',
    'utm_city'=> 'Регион по метке(utm_city)'
);
/**
 * Метки внутреней статстики
 */
$statMarks = array(
    'ip'=>'IP Клиента',
    'city'=>'Регион по IP',
    'referer'=>'Источник перехода'
);

/**
 * ниже поля форм на сайте
 * В этой версии сделана привязка полей к конкретной форме.
 * @use forms.json
 * @use fields.json
 */
$fields = array(
    'name',
    'phone',
    'email',
    'message',
    'formid'=>'Заполненая форма');
//Поля для отправки в смс
$smsfields = array(
   'name'=>'Имя',
   'phone'=>'Телефон', 
   'email', 
);

$fields = array_merge($fields, $utmmarks);
$fields = array_merge($fields, $statMarks);

/**
 * Имена форм
 * С версии 0.3 берутся из forms.json
 * @deprecated
 */
$formnames = array(
  'не определено',   
);
 
/**
 * Правила валидации
 */  
$rules = array(
   0=>array(
     'name'=>array('maxLenght'=>20)
   )
);

 /**
  * Настройки SMSWEB
  */
$sms = array(
  'login' => '',
  'pass'  => '',
  'from'  => 'INFORM',
  'to'    => '');
?>
