<?php

/**
 * @package Easyland
 * @version 0.3.0
 */
class Form{
  protected $fields;
  public $action;
  protected $mods;
  protected $params;
  protected $name;
  private $_src;
  
  function __construct($id=0) {
    global $formsrc;
    $this->_src = Arr::merge($formsrc[0],$formsrc[$id]);
    $this->id = $id;
    $this->method = $this->_src['method'];
    $this->action = $this->_src['target'];
    $this->submit = $this->_src['submit'];
    $this->name   = $this->_src['name'];
    $this->params = array('form'=>  $this->_src['params']);
    foreach ($this->_src['fields'] as $id){
      $this->fields[] = Field::getInstance($id);
    }
  }
  public static function getInstance($id){
    return new Form($id);
  }
  public function get($id){
    return self::getInstance($id);
  }

  public static function get_hidden_bem(){
    global $formsrc;
    $res = array();
    foreach($formsrc as $id => $form){
      if(array_key_exists('hidden',$form))
      {
        $res[]=self::get($id)->bem();
      }
    }
    return $res;
  }

  public function start(){    
    ?>
<form id="<?php echo $this->name.'-'.$this->id ?>" class="form form_async form_size_<?php echo Arr::get($this->_src,'size','x') ?> form_theme_<?php echo Arr::get($this->_src,'theme','normal') ?> i-bem" method="<?php echo $this->method ?>" action="<?php echo $this->action ?>" data-bem="<?php echo BEM::jsattr($this->params); ?>">
  <input type="hidden" name="formid" value="<?php echo $this->id ?>"/>  
<?php  
    global $utmmarks;
    statistic($utmmarks);
  }
  public function end(){
    echo "</form>";
  }

  /**
   * Return form's block
   * no render
   */
  public function bem(){
    $this->_b = array(
      'block'=>'form',
      'mix'=>['block'=>'mt','js'=>['event'=>'submit']],
      'attrs'=>[
        'action'=>$this->action,
        'method'=>$this->method,
        'id'=>$this->name.'-'.$this->id,
      ],
      'js'=>$this->_src['params'],
      'mods'=>[
        'async'=>$this->_src['async'],
        'size'=>$this->_src['size'],
        'theme'=>$this->_src['theme']?$this->_src['theme']: 'normal',
        'hidden'=>$this->_src['hidden']? true: false,
        'n'=>  $this->get_id()
      ],
      'content'=>[
        ['block'=>'input','type'=>'hidden','name'=>'formid','val'=>$this->id]
      ]
    );

    if(array_key_exists('header', $this->_src)){
      $this->_b['content'][] = [
        'elem'=>'header',
        'content'=>[
          [
            'elem'=>'heading',
            'content'=>$this->_src['header']['main'],
          ],
          [
            'elem'=>'heading',
            'mods'=>['sub'=>true],
            'content'=>$this->_src['header']['sub'],
          ]
        ]
      ];
    } 

    foreach ($this->fields as $field){
        $f[] = $field->bem();
    }
    $this->_b['content'][] = [
      'elem'=>'body',
      'content'=>$f,
      ];
    $this->_b['content'][] = array(
      [
        'block'=>'button',
        'type'=>'submit',
        'mods'=>['size'=>Arr::get($this->_src,'size','x'),'cs'=>'pink','type'=>'submit'],
        'mix'=>[['block'=>'form','elem'=>'submit']],
        'js'=>true,
        'text'=>$this->submit
      ]
    );

    $this->_b['content'][] = array(
      'elem'=>'respond'
    );

    return $this->_b;
  }
  
  /**
   * Возвращает заголовок формы
   * @param bool $sub заголовок или подзаголовок формы
   * @return string 
   */
  public function heading($sub = false) {
    return $sub? $this->_src['header']['sub']: $this->_src['header']['main'];
  }
  
  /**
   * Возвращает массив полей в bemjson-like формате
   * @return array fields
   */
  public function fields(){
    $f = array();
    foreach ($this->fields as $field){
      $f[] = $field->bem();
    }
    return $f;
  }
  
  /**
   * Возвращает текст кнопки
   * @return string submit text
   */
  public function submit(){
   return $this->submit;
  }
  function id(){
    return "#".$this->name.'-'.$this->id;
  }
  function get_id(){
    return $this->name.'-'.$this->id;
  }
  /**
   * Возвращает название формы
   * @return string formname
   */
  function get_name(){
    return $this->_src['formname'];
  }
  /**
   * 
   * @return object Field
   */
  function get_fields(){
    return $this->fields;
  }
}
