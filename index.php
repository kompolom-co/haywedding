<?php
/**
 * Site entry point
 * @package Haywedding
 * @category Haywedding
 */

require_once "vendor/autoload.php";
require_once 'settings.php';
require_once 'lib/init.php';

$bundle = 'index';

$page = (include "bem/desktop.bundles/$bundle/$bundle.php");

if (php_sapi_name() == 'cli-server') {
      file_put_contents("bem/desktop.bundles/$bundle/$bundle.bemjson.js", '('.json_encode($page, JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT).')');
          exec('bem make');
}

//Load templates
require "bem/desktop.bundles/$bundle/$bundle.bh.php";


echo $bh->apply($page);

