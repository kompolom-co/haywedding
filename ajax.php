<?php

/**
 * Ajax entry point
 * @package Haywedding
 */

header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
require_once "vendor/autoload.php";
require 'lib/init.php';
require "bem/desktop.bundles/index/index.bh.php";

/**
 * Requested block
 */
$req_block = array_key_exists('b', $_REQUEST)? $_REQUEST['b'] : false;
/**
 * Id блока
 */
$req_id = array_key_exists('id', $_REQUEST)? $_REQUEST['id'] : false;
/**
 * В какое поле сохранять данные
 */
$data_field = array_key_exists('f', $_REQUEST)? $_REQUEST['f'] : 'content';

if (!$req_block) {
    Send_err(500, 'Block not specified');
}

/**
 * Формат, в котором нужно вывести данные
 */
$format = array_key_exists('json', $_REQUEST)? 'json' : 'html';


/* Lookup block */
$block = array_key_exists($req_block, $content)? $content[$req_block] : false;

if (!$block) {
    Send_err(404, 'Not Found');
}

if ($req_id) {
    $req_id--;
    $data = array_key_exists($req_id, $block)?
      $block[$req_id] :
      Send_err(404, 'block offset not found');
} else {
    $data =  $block;
}

/*Send block*/

if ($format == "json") {
    header("Content-type: text/$format; charset=utf-8");
    exit(json_encode($data, JSON_UNESCAPED_UNICODE));
}

if ($format == "html") {
    header("Content-type: text/$format; charset=utf-8"); 
    $b = ['block'=>$req_block];
    $bemjson = is_array($data)?
      array_merge($b, $data) :
      array_merge($b, [$data_field=>$data]);
    echo $bh->apply($bemjson);
    exit();
}

/**
 * Send HTTP error code
 * @param  int    $code    HTTP error code
 * @param  stirng $comment error text
 * @return void
 */
function Send_err($code, $comment='') 
{
    header("HTTP/1.1 $code $comment");
    exit;
}
