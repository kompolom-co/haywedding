<?php
return [
    block => 'page',
    title => 'Организация армянских свадеб',
    favicon => '/favicon.ico',
    head => [
        [ elem => 'meta', attrs => [ name => 'description', content => '' ] ],
        [ elem => 'meta', attrs => [ name => 'viewport', content => 'width=1024' ] ],
        [ elem => 'css', url => 'http://fonts.googleapis.com/css?family=Open+Sans:400|PT+Serif|Lobster|PT+Serif+Caption:400italic&subset=latin,cyrillic' ],
        [ elem => 'css', url => 'bem/desktop.bundles/index/_index.css' ],
    ],
    scripts=> [[ elem => 'js', url => 'bem/desktop.bundles/index/_index.js' ]],
    content => [
      [
        block=> 'section',
        name=>'topline',
        content=> [
          [
            block=> 'row',
            mods=>[svat=>true],
            content=>[
              [
                elem=>'col',
                mods=> [sw=> 4],
                content=>[
                  [
                    block=> 'site-desc',
                    content=>$content['site-desc']
                  ],
                ]
              ],
              [
                elem=>'col',
                mods=> [sw=> 4, sar=> true],
                content=>[
                  [
                    block=> 'logo',
                    content=>[
                      [
                        block=> 'image',
                        url=> 'media/logo.png',
                        alt=> 'Hay Wedding'
                      ],
                      [
                        elem=> 'desc',
                        content=>$content['logo-desc']
                      ]
                    ]
                  ]
                ]
              ],
              [
                elem=>'col',
                mods=> [sw=> 4],
                content=>[
                  [
                    block=> 'language-selector',
                    content=>[
                      [
                        block=>'lang',
                        content=>[
                          [
                            block=> 'link',
                            url=> '/ru',
                            content=>[
                              [
                                block=>'lang',
                                elem=> 'icon',
                                mods => ['l'=>'ru'],
                              ],
                              [
                                block=>'lang',
                                elem=> 'name',
                                content=> 'Рус'
                              ]
                            ]
                          ],
                        ]
                      ], //lang
                      [
                        block=>'lang',
                        content=>[
                          [
                            block=> 'link',
                            url=> '/arm',
                            content=>[
                              [
                                block=>'lang',
                                elem=> 'icon',
                                mods => ['l'=>'arm'],
                              ],
                              [
                                block=>'lang',
                                elem=> 'name',
                                content=> 'ՀԱՅ'
                              ]
                            ]
                          ],
                        ]
                      ], //lang
                    ]
                  ],
                  [
                    block=> 'contacts',
                    content=> [
                      [
                        block=> 'contact',
                        type=> 'email',
                        content=>$content['email'],
                      ],
                      [
                        block=> 'contact',
                        type=> 'phone',
                        content=> $content['phone']
                      ],
                      [
                        block=> 'contact',
                        type=> 'callback',
                        content=> $content['callback'],
                        url=> Form::get(1)->id(),
                      ]
                    ]
                  ],
                ]
              ],
            ]
          ],
        ]
      ],
      [
        block=> 'menu-container',
        content=> [
          [
            block=> 'nav-menu',
            items=> $content['nav-menu']
          ]
        ]
      ],
      [
          'block'=>'slider',
          'js'=>['pager'=>false,'auto'=>true, 'pause'=>10000, 'autoHover'=>true, 'mode'=> 'horizontal'],
          'content'=>[
              [
                  'block'=>'slide',
                  'url'=>'/media/slide2.jpg',
                  'content'=>[
                    [
                      elem=>'col',
                      mods=>['sw'=>5,'so'=>7],
                      content=>[
                            [
                                'block'=>'form',
                                'mods'=>['theme'=>'wedding','async'=>true,'size'=>'l'],
                                'action'=> Form::get(3)->action,
                                'id'=>Form::get(3)->get_id(),
                                'method'=>Form::get(3)->method,
                                'content'=>[
                                    [
                                      'block'=>'input',
                                      'type'=>'hidden',
                                      'name'=>'formid',
                                      'val'=>Form::get(3)->id,
                                    ],
                                    [
                                        'elem'=>'header',
                                        'content'=>[
                                            [
                                              'block'=>'heading',
                                              'lvl'=>2,
                                              'mix'=>['block'=>'form','elem'=>'heading'],
                                              'mods'=>['cs'=>'blue','deep'=>true],
                                              'content'=>Form::get(3)->heading()
                                            ],
                                            [
                                               'elem'=>'heading',
                                               'mods'=>['sub'=>true],
                                               'content'=>Form::get(3)->heading(true)
                                            ]
                                        ]
                                    ],
                                    [
                                        'elem'=>'body',
                                        'content'=>[
                                              Form::get(3)->fields(),
                                              [
                                                'block'=>'button',
                                                'mix'=>['block'=>'form', elem=>'submit'],
                                                'mods'=>['theme'=>'wedding','cs'=>'pink','size'=>'l'],
                                                attrs=>['type'=>'submit'],
                                                'text'=>Form::get(3)->submit()
                                              ]
                                            ]
                                    ],
                                    [
                                        'elem'=>'respond'
                                    ]
                                ]
                            ]//form
                      ]
                    ]
                  ]
              ],
              [
                  'block'=>'slide',
                  'url'=>'/media/slide1.jpg',
                  'content'=>[
                      [
                        'elem'=>'col',
                        'mods'=>['sw'=>7],
                        'content'=>[
                            [
                                'block'=>'superaction',
                                'content'=>[
                                    [
                                        block=>'heading',
                                        mix=>[block=>'superaction',elem=>'heading'],
                                        'lvl'=>1,
                                        mods=>['cs'=>'pink','deep'=>true],
                                        content=>$content['superaction']['heading']
                                    ],
                                    array_map(function($i){
                                      return
                                      [
                                        elem=> 'content',
                                        content=> $i
                                      ];
                                    }, $content['superaction']['content']),
                                ],
                            ]//superaction
                        ]
                      ],
                      [
                        'elem'=>'col',
                        'mods'=>['sw'=>5],
                        'content'=>[
                            [
                                'block'=>'form',
                                'mods'=>['theme'=>'wedding','async'=>true,'size'=>'l'],
                                'action'=> Form::get(3)->action,
                                'id'=>Form::get(3)->get_id(),
                                'method'=>Form::get(3)->method,
                                'content'=>[
                                    [
                                      'block'=>'input',
                                      'type'=>'hidden',
                                      'name'=>'formid',
                                      'val'=>Form::get(3)->id,
                                    ],
                                    [
                                        'elem'=>'header',
                                        'content'=>[
                                            [
                                              'block'=>'heading',
                                              'lvl'=>2,
                                              'mix'=>['block'=>'form','elem'=>'heading'],
                                              'mods'=>['cs'=>'blue','deep'=>true],
                                              'content'=>Form::get(3)->heading()
                                            ],
                                            [
                                               'elem'=>'heading',
                                               'mods'=>['sub'=>true],
                                               'content'=>Form::get(3)->heading(true)
                                            ]
                                        ]
                                    ],
                                    [
                                        'elem'=>'body',
                                        'content'=>[
                                              Form::get(3)->fields(),
                                              [
                                                'block'=>'button',
                                                'mix'=>['block'=>'form', elem=>'submit'],
                                                'mods'=>['theme'=>'wedding','cs'=>'pink','size'=>'l'],
                                                attrs=>['type'=>'submit'],
                                                'text'=>Form::get(3)->submit()
                                              ]
                                            ]
                                    ],
                                    [
                                        'elem'=>'respond'
                                    ]
                                ]
                            ]//form
                        ]
                      ]
                      
                  ]
              ],
          ]            
      ], 
      [
        block=> 'section',
        name=> 'why',
        mods=>[cs=>'pink'],
        content=>[
          [
            block=> 'why',
            content=>[
              [
                block=> 'why-list',
                items=>$content['why-list']
              ]
            ]
          ]
        ]
      ],
      [
        block=> 'section',
        name=> 'service',
        content=> [
          [
            block=> 'heading',
            lvl=> 2,
            mods=> [cs=>'pink', deep=> true],
            content=> $content['service']
          ],
          [
            block=> 'service-list',
            'lang' => $lang,
            items=> $content["service-desc"]
          ]
        ]
      ],
      [
        block=> 'section',
        name=> 'why-we',
        mods=> [cs=> 'blue'],
        content=> [
          [
            block=> 'heading',
            lvl=> 2,
            mods=> [cs=> 'blue',deep=>true],
            content=> $content['why-we']['name'],
          ],
          [
            block=> 'why-we',
            items=> $content['why-we']['items']
          ]
        ]
      ],
      [
        block=> 'section',
        name=> 'action',
        content=> [
          [
            block=> 'row',
            mods=>[svam=>true],
            mix=> [block=>'action'],
            content=> [
              [
                elem=> 'col',
                mods=> [sw=>4],
                mix=> [block=> 'action', elem=> 'desc'],
                content=> [
                  [
                    block=> 'heading',
                    lvl=> 2,
                    mods=> [cs=> 'pink', deep=> true],
                    content=> $content['action']['name']
                  ],
                  [
                    block=> 'action',
                    elem=> 'text',
                    content=> array_map(function($i){
                      return
                      [
                        tag=> "p",
                        content=> $i
                      ];
                    }, $content['action']['text'])
                  ]
                ]
              ],
              [
                elem=>'col',
                mods=>[sw=> 4],
                content=>[
                  [
                    block=>'action',
                    elem=> 'photo'
                  ]
                ]
              ],
              [
                elem=> 'col',
                mods=> [sw=> 4],
                content=> [
                  [
                    'block'=>'form',
                    'mods'=>['async'=>true],
                    'mix'=>['block'=>'action', 'elem'=>'form'],
                    'action'=> Form::get(5)->action,
                    'id'=>Form::get(5)->get_id(),
                    'method'=>Form::get(5)->method,
                    'content'=>[
                        [
                          'block'=>'input',
                          'type'=>'hidden',
                          'name'=>'formid',
                          'val'=>Form::get(5)->id,
                        ],
                        [
                            'elem'=>'header',
                            'content'=>[
                                [
                                  'block'=>'heading',
                                  'lvl'=>3,
                                  'mix'=>['block'=>'form','elem'=>'heading'],
                                  'mods'=>['cs'=>'blue','deep'=>true],
                                  'content'=>Form::get(5)->heading()
                                ],
                                
                            ]
                        ],
                        [
                            'elem'=>'body',
                            'content'=>[
                                  Form::get(5)->fields(),
                                  [
                                    'block'=>'button',
                                    'mix'=>['block'=>'form', elem=>'submit'],
                                    'mods'=>['theme'=>'wedding','cs'=>'pink','type'=>'submit','size'=>'x'],
                                    'text'=>Form::get(5)->submit()
                                  ]
                                ]
                        ],
                        [
                            'elem'=>'respond'
                        ]
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ],
      [
        block=>'section',
        name=> 'wonder',
        mods=>[cs=>'pink'],
        content=>[
          [
            block=>'heading',
            lvl=> 2,
            mods=>[cs=>'blue', deep=> true],
            content=> $content['wonder']['name']
          ],
          [
            block=> 'wonder',
            steps=> $content['wonder']['steps']
          ]
        ]
      ],
      [
        block=> 'section',
        name=> 'pricing',
        content=> [
          [
            block=>'heading',
            lvl=> 2,
            mods=> [cs=> 'pink', deep=> true],
            mix=> [block=>'pricing',elem=>'heading'],
            content=>$content['pricing'][0]
          ],
          [
            block=> 'pricing',
            content=>[
              [
                tag=> 'p',
                content=> $content['pricing'][1]
              ],
              [
                elem=> 'pig'
              ],
              [
                tag=> 'p',
                content=> $content['pricing'][2]
              ]
            ]
          ]
        ]
      ],
      [
        block=> 'section',
        name=> 'reasons',
        mods=> [cs=> 'blue'],
        content=> [
          [
            block=>'reasons',
            mix=> [block=>'row'],
            content=> [
              [
                block=>'heading',
                lvl=> 2,
                mods=> [cs=>'blue', deep=> true, size=>'s'],
                mix=> [[block=>'reasons', elem=>'heading'],[block=> 'row', elem=>'col', mods=>[sw=>12]]],
                content=> $content['reasons-name']
              ],
              [
                elem=> 'content',
                mix=> [block=>'row', elem=>'col', mods=>[sw=>6]],
                content=>[
                  [
                    elem=> 'items',
                    items=>$content['reasons']
                  ]
                ]
              ],
              [
                elem=> 'photo',
                mix=> [block=>'row', elem=>'col', mods=>[sw=>6]],
              ]
            ]
          ]
        ]
      ],
      [
        block=>'section',
        name=>'questions',
        content=>[
          [
            block=>'questions',
            content=>[
              [
                block=>'heading',
                lvl=>2,
                content=> $content['questions']['name']
              ],
              [
                elem=> 'content',
                mix=> [block=>'row'],
                content=>[
                  [
                    elem=> 'text',
                    mix=> [block=>'row', elem=>'col', mods=>[sw=>7]],
                    content=>[
                      [
                        tag=>'p',
                        content=> $content['questions']['texta']
                      ],
                      [
                        elem=> 'phone',
                        content=> $content['phone']
                      ],
                      [
                        tag=> 'p',
                        content=> $content['questions']['textb']
                      ]
                    ]
                  ],
                  [
                    block=> 'row',
                    elem=> 'col',
                    mods=> [sw=> 5],
                    content=> [
                      [
                        Form::get(6)->bem()
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ],
      [
        block=>'section',
        name=> 'ready',
        content=>[
          [
            block=>'heading',
            lvl=> 2,
            mods=> [cs=> 'pink', deep=> true],
            content=> $content['ready']['name']
          ],
          [
            block=>'ready',
            content=>[
              array_map(function($text){
                return 
                [
                  tag=>'p',
                  content=> $text,
                ];
              }, $content['ready']['text']),
              [
                block => 'heading',
                'lvl' => 3,
                mods => ['cs'=>'blue'],
                content => $content['ready']['coord'] 
              ],
              [
                block => 'rvars',
                mods => ['lang' => $lang? $lang: false],
                content => array_map(function($item){
                  return
                  [
                    block => 'rvar',
                    mods => ['color'=> $item['cs']],
                    content => [
                      [
                        elem => 'head',
                        content => $item['head'] 
                      ],
                      [
                        block => 'list',
                        mix => ['block' => 'rvar', elem => 'includes'],
                        content => array_map(function($item){
                          return [
                            elem => 'item',
                            content => $item
                          ];
                        }, $item['items']) 
                      ],
                      [
                        elem => 'foot',
                        content => [
                          $item['lbl'].':',
                          [
                            elem => 'price',
                            content => $item['price']
                          ],
                          $item['curr']
                        ]
                      ]
                    ]
                  ]; //rvar
                }, $content['ready']['rvars'])
              ],//rvars
              [
                tag=>'p',
                content=> $content['ready']['textb']
              ],
              [
                block=>'button',
                mix=>[block=>'ready',elem=>'button'],
                mods=>[theme=> 'wedding',size=>'xl', cs=> 'pink', 'action'=>'lightbox'],
                text=> $content['ready']['order'],
                url=>Form::get(4)->id()
              ],
              [
                block=>'footnote',
                content=> $content['ready']['note'] 
              ]
            ]
          ]
        ]
      ],
      [
        block=>'section',
        name=>'portfolio',
        mods=> [cs=> 'blue'],
        content=>[
          [
            block=>'heading',
            lvl=> 2,
            mods=> [cs=>'blue', deep=>true],
            content=> $content['portfolio']['name']
          ],
          array_map(function($rev){
            return [
            block=>'case',
            content=> [
              [
                elem=>'names',
                content=>[
                  [elem=>'name', content=>$rev['names'][0]],
                  '&nbsp;'.$rev['rev-and'].'&nbsp;',
                  [elem=>'name', content=>$rev['names'][1]],
                ]
              ],
              [
                elem=>'photo-wrap',
                content=>[
                    elem=>'photos',
                    'path'=>$rev['path'],
                    'photos'=>$rev['photos']
                ]
              ],
              [
                block=>'review',
                content=>[
                  [
                    elem=> 'text',
                    content=>$rev['text']
                  ],
                  [
                    elem=> 'author',
                    content=> $rev['author']
                  ]
                ]
              ],
              !empty($rev['video'])?[
                block=>'wedding-video',
                content=>[
                  [
                    block=> 'video',
                    mods=> [vendor=>'youtube'],
                    id=> $rev['video']
                  ]
                ]
              ]:null,
            ]
          ];
          },$content['reviews']),
        ]
      ],
      [
        block => 'section',
        name => 'reviews',
        content => array_map(function($review, $key){
          return [
              block => 'row',
              mods => ['svam' => true, 'reverse' => $key%2? true : false],
              mix => ['block' => 'review', mods => ['pos' => $key%2? 'even': 'odd', 'size' => 's']],
              content => [
                  [
                    elem => 'col',
                    mods => ['sw' => 4, 'sol'=>$key%2? true : false],
                    content => [
                      [
                        block => 'review',
                        elem => 'imgwrap',
                        content => [
                          [
                            block => 'image',
                            'url' => $review['img']
                          ]
                        ]
                      ]
                    ]
                  ], //col
                  [
                    elem => 'col',
                    mods => ['sw' => 8],
                    content => [
                      [
                        block => 'review',
                        elem => 'names',
                        content => $review['names']
                      ],
                      [
                        block => 'review',
                        elem => 'text',
                        content => $review['text']
                      ],
                      [
                        block => 'review',
                        elem => 'author',
                        content => $review['author']
                      ]
                    ]
                  ], //col
              ]
          ];
        }, $content['other-reviews'], array_keys($content['other-reviews']))
      ], //section reviews
      [
        block=>'section',
        'name' =>'lead',
        content=>[
          [
            block=>'row',
            mods=>[svat=> true],
            content=>[
              [
                elem=> 'col',
                mods=>[so=>7, sw=>5],
                content=>[
                  [
                      'block'=>'form',
                      'mods'=>['theme'=>'wedding','async'=>true,'size'=>'l'],
                      'action'=> Form::get(3)->action,
                      'id'=>Form::get(3)->get_id(),
                      'method'=>Form::get(3)->method,
                      'content'=>[
                          [
                            'block'=>'input',
                            'type'=>'hidden',
                            'name'=>'formid',
                            'val'=>Form::get(3)->id,
                          ],
                          [
                              'elem'=>'header',
                              'content'=>[
                                  [
                                    'block'=>'heading',
                                    'lvl'=>2,
                                    'mix'=>['block'=>'form','elem'=>'heading'],
                                    'mods'=>['cs'=>'blue','deep'=>true],
                                    'content'=>Form::get(3)->heading()
                                  ],
                                  [
                                     'elem'=>'heading',
                                     'mods'=>['sub'=>true],
                                     'content'=>Form::get(3)->heading(true)
                                  ]
                              ]
                          ],
                          [
                              'elem'=>'body',
                              'content'=>[
                                    Form::get(3)->fields(),
                                    [
                                      'block'=>'button',
                                      'mix'=>['block'=>'form', elem=>'submit'],
                                      'mods'=>['theme'=>'wedding','cs'=>'pink','size'=>'l'],
                                      attrs=>['type'=>'submit'],
                                      'text'=>Form::get(3)->submit()
                                    ]
                                  ]
                          ],
                          [
                              'elem'=>'respond'
                          ]
                      ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ],
      [
        block=> 'section',
        name=>'topline',
        content=> [
          [
            block=> 'row',
            mods=>[svam=>true],
            content=>[
              [
                elem=>'col',
                mods=> [sw=> 4],
                content=>[
                  [
                    block=> 'site-desc',
                    content=>$content['site-desc']
                  ],
                ]
              ],
              [
                elem=>'col',
                mods=> [sw=> 4, sar=> true],
                content=>[
                  [
                    block=> 'logo',
                    content=>[
                      [
                        block=> 'image',
                        url=> 'media/logo.png',
                        alt=> 'Hay Wedding'
                      ],
                      [
                        elem=> 'desc',
                        content=>$content['logo-desc']
                      ]
                    ]
                  ]
                ]
              ],
              [
                elem=>'col',
                mods=> [sw=> 4],
                content=>[
                  [
                    block=> 'contacts',
                    content=> [
                      [
                        block=> 'contact',
                        type=> 'email',
                        content=>$content['email'],
                      ],
                      [
                        block=> 'contact',
                        type=> 'phone',
                        content=> $content['phone']
                      ],
                      [
                        block=> 'contact',
                        type=> 'callback',
                        content=> $content['callback'],
                        url=> Form::get(1)->id(),
                      ]
                    ]
                  ],
                ]
              ],
            ]
          ],
        ]
      ],
      [
        block=> 'social',
        links=>[
          [
            name=>'vk',
            url=>'https://vk.com/haywedding1'
          ],
          [
            name=>'ok',
            url=>'http://ok.ru/haywedding'
          ],
          [
            name=>'in',
            url=>'http://instagram.com/hay_wedding/'
          ]
        ]
      ], //social
      [
          block=>'hidden',
          content=>[
              Form::get_hidden_bem()
          ]
      ],
      [
          block=>'metrica',
          'id' =>METRICA
      ]
    ]
];
