({
    block : 'page',
    title : 'Организация армянских свадеб',
    favicon : '/favicon.ico',
    head : [
        { elem : 'meta', attrs : { name : 'description', content : '' } },
        { elem : 'meta', attrs : { name : 'viewport', content : 'width=device-width, initial-scale=1' } },
        { elem : 'css', url : 'http://fonts.googleapis.com/css?family=PT+Serif|Lobster|PT+Serif+Caption:400italic&subset=latin,cyrillic' },
        { elem : 'css', url : 'bem/desktop.bundles/index/_index.css' },
    ],
    scripts: [{ elem : 'js', url : 'bem/desktop.bundles/index/_index.js' }],
    content : [
      {
        block: 'section',
        name:'topline',
        content: [
          {
            block: 'row',
            mods:{svam:true},
            content:[
              {
                elem:'col',
                mods: {mw: 4},
                content:[
                  {
                    block: 'site-desc',
                    content:'Организация <em>Армянских свадеб</em> в москве и МО'
                  },
                ]
              },
              {
                elem:'col',
                mods: {mw: 4, sar: true},
                content:[
                  {
                    block: 'logo',
                    content:[
                      {
                        block: 'image',
                        url: 'media/logo.png',
                        alt: 'Hay Wedding'
                      },
                      {
                        elem: 'desc',
                        content:'Свадебное агенство'
                      }
                    ]
                  }
                ]
              },
              {
                elem:'col',
                mods: {mw: 4},
                content:[
                  {
                    block: 'language-selector',
                    content:[
                      {
                        block:'lang',
                        content:[
                          {
                            block: 'link',
                            url: '/ru',
                            content:[
                              {
                                block:'lang',
                                elem: 'icon'
                              },
                              {
                                block:'lang',
                                elem: 'name',
                                content: 'Рус'
                              }
                            ]
                          },
                          {
                            block: 'link',
                            url: '/arm',
                            content:[
                              {
                                block:'lang',
                                elem: 'icon'
                              },
                              {
                                block:'lang',
                                elem: 'name',
                                content: 'арм'
                              }
                            ]
                          },
                        ]
                      }
                    ]
                  },
                  {
                    block: 'contacts',
                    content: [
                      {
                        block: 'contact',
                        type: 'email',
                        content:'haywedding@gmial.com',
                      },
                      {
                        block: 'contact',
                        type: 'phone',
                        content: '+7 (495) 766-54-68'
                      },
                      {
                        block: 'contact',
                        type: 'callback',
                        content: 'Закажите обратный звонок',
                        url: '#call-form',
                      }
                    ]
                  },
                ]
              },
            ]
          },
        ]
      },
      {
        block: 'menu-container',
        content: [
          {
            block: 'nav-menu',
            items: [
              { name: 'Что мы делаем', url: '#section-service' },
              { name: 'Как мы работаем', url: '#section-wonder' },
              { name: 'Стоимость', url: '#section-pricing' },
              { name: 'Портфолио', url: '#section-portfolio' },
            ]
          }
        ]
      },
      {
        block: 'section',
        name: 'header',
        content:[
        ]
      }, 
      {
        block: 'section',
        name: 'why',
        mods:{cs:'pink'},
        content:[
          {
            block: 'why',
            content:[
              {
                block: 'why-list',
                items:[
                  {
                    icon: 'clipboard',
                    text: 'Работаем по <br> договору и <b>выполняем <br> все в срок</b>',
                  },
                  {
                    icon: 'diamond',
                    text: '<b>Индивидуальный</b><br> подход',
                  },
                  {
                    icon: 'frame',
                    text: '<b>Учитываем традиции</b> <br> проведения <br> армянской свадьбы',
                  },
                  {
                    icon: 'bage',
                    text: '<b>Учитываем традиции</b> <br> проведения <br> армянской свадьбы',
                  },
                  {
                    icon: 'count',
                    text: '<b>Присутствие <br> организатора</b> <br> в день Вашей свадьбы',
                  },
                  {
                    icon: 'place',
                    text: 'Организуем свадьбу с <br> участием <b>до 1000 персон</b>',
                  },
                ]
              }
            ]
          }
        ]
      },
      {
        block: 'section',
        name: 'service',
        content: [
          {
            block: 'heading',
            lvl: 2,
            mods: {cs:'pink', deep: true},
            content: 'Что мы делаем?'
          },
          {
            block: 'service-list',
            items: [
              {
                icon: 's1',
                text: 'Выбор места проведения торжества',
              },
              {
                icon: 's2',
                text: 'Декор и оформление'
              },
              {
                icon: 's3',
                text: 'Выездная регистрация'
              },
              {
                icon: 's4',
                text: 'Подбор ведущего'
              },
              {
                icon: 's5',
                text: 'Подбор музыкантов'
              },
              {
                icon: 's6',
                text: 'Подбор шоу-программы'
              },
              {
                icon: 's7',
                text: 'Подбор фото- и видео-оператора'
              },
              {
                icon: 's8',
                text: 'Подбор свадебного кортежа'
              },
              {
                icon: 's9',
                text: 'Свадебный торт'
              },
              {
                icon: 's10',
                text: 'Подбор визажиста и парикмахера'
              },
              {
                icon: 's11',
                text: 'Постановка свадебного танца'
              },
              {
                icon: 's12',
                text: 'Детский аниматор'
              },
            ]
          }
        ]
      },
      {
        block: 'section',
        name: 'why-we',
        mods: {cs: 'blue'},
        content: [
          {
            block: 'heading',
            lvl: 2,
            mods: {cs: 'blue',deep:true},
            content: 'Почему выбирают нас?',
          },
          {
            block: 'why-we',
            items: [
              {
                name: 'Команда профессионалов',
                text: 'Внимание к каждой мелочи и богатый опыт в любимом деле станут гарантией проведения свадебного торжества по высшему разряду',
              },
              {
                name: 'Традиции армянской свадьбы',
                text: 'Мы знаем все традиции армянской свадьбы начиная с выкупа невесты заканчивая вручением таросиков и с радостью учитываем их при организации свадьбы!',
              },
              {
                name: 'Надежные поставщики',
                text: 'Мы работаем только с проверенными поставщиками, которые уже успешно зарекомендовали себя на рынке свадебных услуг.',
              },
              {
                name: 'Свадебный распорядитель',
                text: 'В день вашего торжества мы предоставляем свадебного распорядителя, который берет все хлопоты на себя. Вам остается только наслаждаться праздником!',
              },
              {
                name: 'Рациональное использование средств',
                text: 'Наши специалисты помогут Вам принять оптимальное решение, соответствующее Вашим желаниям и возможностям.',
              },
              {
                name: 'Организация свадеб под ключ',
                text: 'Полная организация Вашей свадьбы: от создания идеи до ее реализации. Мы берем на себя решение всех возникающих вопросов и задач от А до Я.',
              }
            ]
          }
        ]
      },
      {
        block: 'section',
        name: 'action',
        content: [
          {
            block: 'row',
            mods:{svam:true},
            mix: {block:'action'},
            content: [
              {
                elem: 'col',
                mods: {sw:4},
                mix: {block: 'action', elem: 'desc'},
                content: [
                  {
                    block: 'heading',
                    lvl: 2,
                    mods: {cs: 'pink', deep: true},
                    content: 'Акция!'
                  },
                  {
                    block: 'action',
                    elem: 'text',
                    content: [
                      {
                        tag: 'p',
                        content:'Фотосессия для невесты и ее подружек в <em>подарок!</em>'
                      },
                      {
                        tag: 'p',
                        content:'Оставьте заявку на организацию свадьбы и получите фотосет в <b>подарок!</b>'
                      }
                    ]
                  }
                ]
              },
              {
                elem:'col',
                mods:{sw: 4},
                content:[
                  {
                    block:'action',
                    elem: 'photo'
                  }
                ]
              },
              {
                elem: 'col',
                mods: {sw: 4},
                content: [
                  {
                    block:'form',
                    action: 'order.php',
                    method: 'post',
                    content:[
                      {
                        elem: 'header',
                        mix: {block:'heading', mods:{cs:'blue'}},
                        content: 'Оставьте заявку',
                      },
                      {
                        elem:'body',
                        content:[
                          {
                            block: 'input',
                            type: 'text',
                            name: 'name'
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        block:'section',
        name: 'wonder',
        mods:{cs:'pink'},
        content:[
          {
            block:'heading',
            lvl: 2,
            mods:{cs:'blue', deep: true},
            content:'Как совершается чудо?'
          },
          {
            block: 'wonder',
            steps: [
              {
                icon: 'cup',
                name: '1',
                text: 'встреча с Вами и презентация наших услуг'
              },
              {
                icon: 'photo',
                name: '2',
                text: 'Разработка нескольких вариантов проведения'
              },
              {
                icon: 'pad',
                name: '3',
                text: 'Заключение договора'
              },
              {
                icon: 'box',
                name: '4',
                text: 'Подготовка к свадьбе и выбор поставщиков'
              },
              {
                icon: 'sun',
                name: '5',
                text: 'День проведения свадебного торжества'
              },
            ]
          }
        ]
      },
      {
        block: 'section',
        name: 'pricing',
        content: [
          {
            block:'heading',
            lvl: 2,
            mods: {cs: 'pink', deep: true},
            mix: {block:'pricing',elem:'heading'},
            content:'Стоимость организации свадьбы под ключ'
          },
          {
            block: 'pricing',
            content:[
              {
                tag: 'p',
                content: 'Наши цены полностью прозрачны и фиксированы'
              },
              {
                elem: 'pig'
              },
              {
                tag: 'p',
                content: 'Агентское вознаграждение составляет <em>75 000</em> руб. вне зависимости от концепции и сложности проведения мероприятия'
              }
            ]
          }
        ]
      },
      {
        block: 'section',
        name: 'reasons',
        mods: {cs: 'blue'},
        content: [
          {
            block:'reasons',
            mix: {block:'row'},
            content: [
              {
                block:'heading',
                lvl: 2,
                mods: {cs:'blue', deep: true, size:'s'},
                mix: [{block:'reasons', elem:'heading'},{block: 'row', elem:'col', mods:{sw:12}}],
                content: 'За что вы платите свадебному организатору?'
              },
              {
                elem: 'content',
                mix: {block:'row', elem:'col', mods:{sw:6}},
                content:[
                  {
                    elem: 'items',
                    items:[
                      'Свадебный организатор поможет спланировать свадьбу Вашей мечты, возьмет на себя все организационные моменты и решит любые возникающие сложности.',
                      "Свадебный организатор предоставит Вам подборку лучших ведущих, музыкантов и других специалистов свадебной идустрии, что сэкономит Ваше время и сделает подготовку к свадьбе легкой и непринужденной",
                      "Свадебный организатор поможет Вам рационально распределить выделенную сумму на проведение торжества.",
                      "В день Вашей свадьбы организатор проследит за ходом событий и возьмет все хлопоты на себя, обеспечив Вас спокойствием и уверенностью, что все пройдет по плану и  на высшем уровне."
                    ]
                  }
                ]
              },
              {
                elem: 'photo',
                mix: {block:'row', elem:'col', mods:{sw:6}},
              }
            ]
          }
        ]
      },
      {
        block:'section',
        name:'questions',
        content:[
          {
            block:'questions',
            content:[
              {
                block:'heading',
                lvl:2,
                content: 'Появились вопросы?'
              },
              {
                elem: 'content',
                mix: {block:'row'},
                content:[
                  {
                    elem: 'text',
                    mix: {block:'row', elem:'col', mods:{sw:7}},
                    content:[
                      {
                        tag:'p',
                        content: 'Звоните прямо сейчас'
                      },
                      {
                        elem: 'phone',
                        content:'+7 (495) 766-54-68'
                      },
                      {
                        tag: 'p',
                        content: 'или оставьте заявку на обратный звонок, и  мы Вам перезвоним'
                      }
                    ]
                  },
                  {
                    block: 'row',
                    elem: 'col',
                    mods: {sw: 5},
                    content: [
                      {
                        block: 'form'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        block:'section',
        name: 'ready',
        content:[
          {
            block:'heading',
            lvl: 2,
            mods: {cs: 'pink', deep: true},
            content:'Если Ваша свадьба почти готова...'
          },
          {
            block:'ready',
            content:[
              {
                tag:'p',
                content: '...и в день свадьбы Вы хотите полностью насладиться праздником  общением с Вашими близкими, мы предлагаем Вам <b>услуги по координации свадьбы</b>',
              },
              {
                tag: 'p',
                content: 'В обязанности свадебного координатора входит:'
              },
              {
                elem:'acts',
                acts: [
                  {i:'time', text:'Создание и отслеживание тайминга'},
                  {i:'route', text:'Рассадка гостей'},
                  {i:'meet', text:'Встреча артистов'},
                  {i:'money', text:'Раздача денежных средств специалистам, рассчет с которыми производится в день мероприятия.'},
                ]
              },
              {
                tag:'p',
                content: 'Таким образом, все организационные моменты в день Вашего торжества мы возьмем на себя, благодаря чему Вы и Ваши гости в полной мере насладятся торжеством и почувствуют атмосферу Вашей неповторимой сказки!'
              },
              {
                tag: 'p',
                content:[
                  'Стоимость координации свадьбы - ',
                  {elem: 'price', content:'30 000'},
                  'руб.*'
                ]
              },
              {
                block:'button',
                mods:{theme: 'wedding',size:'xl', cs: 'pink'},
                text: 'Заказать координацию свадьбы',
              },
              {
                block:'footnote',
                content: 'Минимальный состав - два координотора. Количество специалистов в день Вашей свадьбы зависит от площади и количества гостей. Для обеспечения комфортной рассадки Ваших гостей на масштабной свадьбе, мы рекомендуем пригласить дополнительных специалистов. Стоимость каждого последующего координотора - 5&nbsp;000 руб.'
              }
            ]
          }
        ]
      },
      {
        block:'section',
        name:'portfolio',
        mods: {cs: 'blue'},
        content:[
          {
            block:'heading',
            lvl: 2,
            mods: {cs:'blue', deep:true},
            content: 'Портфолио'
          },
          {
            block:'case',
            content: [
              {
                elem:'names',
                content:[
                  {elem:'name', content:'Гарик'},
                  '&nbsp;и&nbsp;',
                  {elem:'name', content:'Сюзанна'},
                ]
              },
              {
                elem:'photos',
                content:[]
              },
              {
                block:'review',
                content:[
                  {
                    elem: 'text',
                    content:'Девочки мои хорошие, еще раз хочу поблагодарить вас за вашу помощь и труд                       ,   на самом деле только в середине своей свадьбы я узнала, что сейчас существуют организаторы свадеб, которые с нуля тебе все сделают,    но не смотря на то,что я с опозданием к вам обратилась и совсем безответственно к этому вопросу подошла, вы свое дело знаете отлично)))и даже моя неопределенность не помешала вам сделать все на высшем уровне))) вы ко мне не просто как к работе отнеслись, а как к своей давней знакомой)))и поэтому мне так легко было с вами!)))одно знаю точно, если кто-то спросит меня кому можно доверить свою свадьбу, я назову ваши имена, дорогие мои Сати и София))спасибо вам за чудесную организацию нашей свадьбы) '
                  },
                  {
                    elem: 'author',
                    content: 'Сюзанна'
                  }
                ]
              },
              {
                block:'wedding-video',
                content:[
                  {
                    block: 'video',
                    mods: {vendor:'youtube'},
                    id: 'LkzUkcjNfzk'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        block:'section',
        name:'lead',
        content:[
          {
            block:'row',
            mods:{sar: true},
            content:[
              {
                elem: 'col',
                mods:{so:6, sw:6},
                content:[
                  {
                    block:'form'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        block: 'section',
        name:'topline',
        content: [
          {
            block: 'row',
            mods:{svam:true},
            content:[
              {
                elem:'col',
                mods: {mw: 4},
                content:[
                  {
                    block: 'site-desc',
                    content:'Организация <em>Армянских свадеб</em> в москве и МО'
                  },
                ]
              },
              {
                elem:'col',
                mods: {mw: 4, sar: true},
                content:[
                  {
                    block: 'logo',
                    content:[
                      {
                        block: 'image',
                        url: 'media/logo.png',
                        alt: 'Hay Wedding'
                      },
                      {
                        elem: 'desc',
                        content:'Свадебное агенство'
                      }
                    ]
                  }
                ]
              },
              {
                elem:'col',
                mods: {mw: 4},
                content:[
                  {
                    block: 'contacts',
                    content: [
                      {
                        block: 'contact',
                        type: 'email',
                        content:'haywedding@gmial.com',
                      },
                      {
                        block: 'contact',
                        type: 'phone',
                        content: '+7 (495) 766-54-68'
                      },
                      {
                        block: 'contact',
                        type: 'callback',
                        content: 'Закажите обратный звонок',
                        url: '#call-form',
                      }
                    ]
                  },
                ]
              },
              {
                elem:'col',
                mods: {sw:12},
                content:[
                  {
                    block: 'social',
                    links:[
                      {
                        name:'vk',
                        url:'https://vk.com/haywedding1'
                      },
                      {
                        name:'ok',
                        url:'http://ok.ru/haywedding'
                      },
                      {
                        name:'in',
                        url:'http://instagram.com/hay_wedding/'
                      }
                    ]
                  }
                ]
              }
            ]
          },
        ]
      },
    ]
})
