/* global modules:false */

modules.define('nav-menu',['jquery','i-bem__dom'], function(provide, $, BEMDOM) {

provide(BEMDOM.decl('nav-menu',{
  onSetMod:{
    'js':{
      'inited': function(){
        this._startpos = this.domElem.offset().top;
        this._fixed = this.hasMod('fixed');
        this.offset = this.domElem.height();
        this.speed = this.params.speed || 800;
        this.bindTo($(window),'scroll', this._onscroll, this);
        BEMDOM.blocks['link'].on(this.domElem,'click', this._scrollto, this);
      }
    },
    'fixed':{
      'true': function(){
        this._fixed = true;
      },
      '': function(){
        this._fixed = false;
      }
    }
  },
  _onscroll: function(){
    var scroll = $(window).scrollTop();
    if ( !this._fixed && scroll > this._startpos ){
      this.setMod('fixed'); 
    }else if ( this._fixed && scroll <= this._startpos ) {
      this.delMod('fixed'); 
    }
  },
  _scrollto: function(e){
    var url = e.target.getUrl();
    var to = $(url).offset().top - this.offset;
    $('html, body').animate( {scrollTop: to}, this.speed );
    return false;
  }
}
));//decl

});

