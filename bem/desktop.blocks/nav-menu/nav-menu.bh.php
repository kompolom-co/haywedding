<?php
return function ($bh) {
 $bh->match("nav-menu", function ($ctx, $json){
   $ctx->applyBase()
       ->js(true)
       ->content([
    'elem'=> 'container',
    'mix'=> ['block'=>'container'],
    'content'=> $ctx->content()
   ],true);
 });
};
