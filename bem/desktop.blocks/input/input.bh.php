<?php
return function ($bh) {
 $bh->match("input", function ($ctx, $json){
   $ctx->applyBase();
   $ctx->content(
     [
        [
          'elem'=>'icon',
          'content'=>[
            'block'=>'icon',
            'mods'=>['i'=>$ctx->param('icon')] 
          ]
        ],
        $ctx->content()
     ],
     true
   );
 });
};
