<?php
return function ($bh) {
 $bh->match("why-list", function ($ctx, $json){
   $ctx->tag('ul');
   $ctx->content(array_map(function($item){
     return [
       'block' => 'why-item',
       'tag' => 'li',
       'content' => [
          [
            'elem'=>'icon',
            'tag'=>'span',
            'cls'=>'w-'.$item['icon']
          ],
          [
            'tag'=>'span',
            'elem'=>'txt',
            'content'=>$item['text']
          ]
       ]
     ];
   }, $json->items));
 });
};
