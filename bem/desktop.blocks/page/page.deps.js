({
    mustDeps: [
      {block:'old'},
      {block: 'fonts', mods:{'PFDinTextCondPro': true}}
    ],
    shouldDeps: [
      {block:'mt'},
      {block:'hidden'},
      {block:'form',mods:{async:true, theme: 'normal'}},
    ]
})
