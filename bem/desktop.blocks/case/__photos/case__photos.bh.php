<?php
return function ($bh) {
 $bh->match("case__photos", function ($ctx, $json){
   $group = $ctx->generateId();
   $path = $ctx->param('path');
   foreach($ctx->param('photos') as $photo){
        $a[]= [
          'block'=>'photo',
          'content'=>[
            'block'=>'link',
            'mix'=>['block'=>'gallery','js'=>true],
            'attrs'=>['rel'=>$group],
            'url'=>$path.$photo,
            'content'=>[
              'block'=>'image',
              'url' => $path.'min_'.$photo,
            ]
          ]
        ];
     }
   $ctx
     ->content($a);
 });
};
