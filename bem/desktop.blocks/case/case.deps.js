({
    mustDeps: [],
    shouldDeps: [
      {elem:'photos'},
      {block:'photo'},
      {block:'gallery'},
    ]
})
