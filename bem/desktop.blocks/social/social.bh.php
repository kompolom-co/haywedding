<?php
return function ($bh) {
 $bh->match("social", function ($ctx, $json){
   $ctx->content(
     [
        [
          'elem'=>'list',
          'tag'=>'ul',
          'content'=>array_map(function($link){
            return [
              'elem'=>'item',
              'tag'=>'li',
              'content'=>[
                [
                  'block'=>'link',
                  'target'=>'_blank',
                  'url'=>$link['url'],
                  'content'=>[
                    'block'=>'social',
                    'tag'=>'span',
                    'elem'=>'icon',
                    'mods'=>['i'=>$link['name']]
                  ]
                ]
              ]
            ];
          }, $ctx->param('links'))
        ]
     ]
   ); 
 });
};
