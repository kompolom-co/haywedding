<?php
return function ($bh) {
 $bh->match("service-item", function ($ctx, $json){
   $id = $ctx->generateId();
   $ctx->tag('li')
       ->content([
          [
            'block'=>'link',
            'mix'=>['block'=>'service-item','elem'=>'link'],
            'mods'=>['action'=>'lightbox'],
            'config'=>['type'=>'ajax'],
            'url'=> '/ajax.php?b=service-desc&id='.$ctx->position().'&lang='.$ctx->tParam('lang'),
            'content'=>[
                [
                  'block'=>'image',
                  'url'=>'/media/'.$ctx->param('icon').'.jpg',
                ],
                [
                  'tag'=>'span',
                  'block'=>'service-item',
                  'elem'=>'txt',
                  'content'=>$ctx->param('text')
                ]
            ]
          ]
       ]);
 });
};
