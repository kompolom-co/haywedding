<?php
return function ($bh) {
 $bh->match("contact_type_email", function ($ctx, $json){
   $ctx->content([
     'block'=>'link',
     'url' => 'mailto:'.$ctx->content(),
     'content'=>$ctx->content(),
   ], true)
      ->tag('address');
 });
};
