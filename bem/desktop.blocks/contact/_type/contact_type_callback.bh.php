<?php
return function ($bh) {
 $bh->match("contact_type_callback", function ($ctx, $json){
   $ctx->content([
     'block'=> 'button',
     'mods'=>['theme'=>'wedding','action'=>'lightbox','cs'=>'blue','size'=>'s'],
     'text'=>$ctx->content(),
     'url'=> $json->url
   ],true);
   $ctx->applyBase();
 });
};
