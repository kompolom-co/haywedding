<?php
return function ($bh) {
 $bh->match("contact", function ($ctx, $json){
   $ctx->mod('type', $ctx->param('type'));
   $ctx->applyBase();
 });
};
