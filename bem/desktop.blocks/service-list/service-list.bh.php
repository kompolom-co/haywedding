<?php
return function ($bh) {

 $bh->match("service-list", function ($ctx, $json){
       $ctx
           ->tag('ul')
           ->tParam('lang', $json->lang)
           ->tParam('pre', $ctx->param('pre'))
           ->tParam('caption', $json->caption)
           ->content(
               array_map(
                  function($item){
                     return [
                        'block' => 'service-item',
                        'icon' => $item['icon'],
                        'text' => $item['text'],
                        'img' => $item['img'],
                        'desc' => $item['desc'],
                     ];
                  }, $json->items
                  )
                  )
           ->applyBase();
     });
};
