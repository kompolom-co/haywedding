<?php
return function ($bh) {
 $bh->match("ready__acts", function ($ctx, $json){
   $ctx->content(
     array_map(
       function($act){
         return [
            [
              'elem'=>'act',
              'tag'=>'li',
              'content'=>[
                [
                  'elem'=>'icon',
                  'tag'=>'i',
                  'cls'=>'ri-'.$act['i']
                ],
                [
                  'elem'=>'text',
                  'content'=>$act['text']
                ]
              ]
            ]
         ];
       },
       $ctx->param('acts')
     )
   )
      ->tag('ul');
 });
};
