<?php
return function ($bh) {
 $bh->match("service-desc", function ($ctx, $json){
   $ctx
     ->content(
       [
         'block'=>'row',
         'mods'=>['svam'=>true],
         'content'=>[
              [
                'elem'=>'col',
                'mods'=>['sw'=>7],
                'mix'=>['block'=>'service-desc','elem'=>'ts'],
                'content'=>[
                  [
                    'block'=>'heading',
                    'lvl'=>3,
                    'mods'=>['cs'=>'pink','deep'=>true],
                    'mix'=>['block'=>'service-desc','elem'=>'name'],
                    'content'=>$ctx->param('text')
                  ],
                  [
                    'block'=>'image',
                    'mix'=>['block'=>'service-desc','elem'=>'img'],
                    'url'  => $ctx->param('img'),
                    'alt'  => $ctx->param('text')
                  ],
                  [
                    'block'=>'service-desc',
                    'elem'=> 'text',
                    'content'=>$ctx->param('desc')
                  ]
                ]
              ],//col
              [
                'elem'=>'col',
                'mods'=>['sw'=>5],
                'content'=>[
                  [
                    'block'=>'service-desc','elem'=>'form-cont',
                    'content'=>[
                      [
                        'block'=>'service-desc',
                        'elem'=>'pre',
                        'content'=> $ctx->param('pre')
                      ],
                      [
                        'block'=>'service-desc',
                        'elem'=>'caption',
                        'content'=> $ctx->param('caption')
                      ],
                      Form::get(7)->bem()
                    ]
                  ]
                 ]
              ],//col
         ],//row
       ]
     );
 });
};
