<?php
return function ($bh) {
 $bh->match("wonder", function ($ctx, $json){
   $ctx->content(array_map(function($item){
     return [
       'block' => 'wonder-item',
       'content' => [
          [
            'elem'=>'icon',
            'tag'=>'i',
            'cls'=>'wr-'.$item['icon']
          ],
          [
            'elem'=>'content',
            'content'=>[
              [
                'elem'=>'name',
                'content'=> $item['name']
              ],
              [
                'tag'=>'span',
                'elem'=>'txt',
                'content'=>$item['text']
              ]
            ]
          ],
       ]
     ];
   }, $json->steps));
 });
};
