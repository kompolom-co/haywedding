<?php
return function ($bh) {
 $bh->match("why-we", function ($ctx, $json){
   $ctx->content( array_map(function($item){
     return [
       'block'=>'why-we-item',
       'content' => [
          [
            'elem'=>'name',
            'content'=>$item['name']
          ],
          [
            'elem'=>'text',
            'content'=> $item['text']
          ]
       ]
     ];
   }, $json->items) );
   $ctx->applyBase();
 });
};
