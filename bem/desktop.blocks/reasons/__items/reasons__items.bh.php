<?php
return function ($bh) {
 $bh->match("reasons__items", function ($ctx, $json){
   $ctx->content(array_map(function($item){
     return [
        'block' => 'reason',
        'content'=>$item
     ];
   }, $json->items));
 });
};
