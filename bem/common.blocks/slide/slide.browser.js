/* global modules:false */

modules.define('slide',['i-bem__dom'], function(provide, BEMDOM) {

provide(BEMDOM.decl('slide', {
  onSetMod:{
    'js': {
      'inited': function(){
        if (this.params.bg) {
          this.domElem.css('background-image', 'url('+this.params.bg+')');
        }
      }
    }
  }
}
));

});

