<?php
return function ($bh) {
 $bh->match("slide", function ($ctx, $json){
   $ctx
     ->tag('li')
     ->isFirst()? 
           $ctx
             ->attr('style', 'background-image:url('.$ctx->param('url').')')
             ->mods(['first'=>true]):
           $ctx->js(['bg'=>$ctx->param('url')]);
   $ctx->mix(['block'=>'slide','elem'=>'image'])
     ->content([
//     [
//       'block'=>'image',
//       'url'=>$ctx->param('url'),
//       
//     ],
     [
       'elem'=>'container',
       'mix'=>['block'=>'container'],
       'content'=>[
         'block'=>'row',
         'mods'=>['svat'=>true],
         'content'=>$ctx->content()
       ]
     ] 
   ],true);
 });
};
