({
    mustDeps: [],
    shouldDeps: [
      {elem: 'list'},
      {elem: 'item'},
      {block: 'link', mods:{pseudo: true}},
    ]
})
